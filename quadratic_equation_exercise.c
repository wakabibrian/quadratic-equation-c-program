#include <stdio.h>
#include <math.h>

int main(){
    float a, b, c, d, root1, root2;

    printf("\nEnter the coefficient of  x^2 (a): ");
    scanf("%f", &a);

    while(a==0){
        printf("The coefficient of  x^2 cannot be 0, try again: ");
        scanf("%f", &a);
    }

    printf("Enter the coefficient of  x (b): ");
    scanf("%f", &b);

    printf("Enter the constant (c): ");
    scanf("%f", &c);


    d = (b*b)-(4*a*c); //Discriminant, d

    if(d==0){
        root1 = root2 = -b/(2*a);
        printf("\nroot1 = root2 = %.2f", root1);

    }
    else if(d>0){
        root1 = (-b+sqrt(d))/(2*a);
        root2 = (-b-sqrt(d))/(2*a);
        printf("\nroot1 = %.2f and root2 = %.2f", root1, root2);
    }
    else{
        
        printf("\nComplex");
    }

    return 0;
}
